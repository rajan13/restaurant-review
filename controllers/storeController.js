const mongoose = require('mongoose');
const Store = mongoose.model('Store');
const multer = require('multer');
const jimp = require('jimp');
const uuid = require('uuid');
const User = mongoose.model('User');

const multerOptions = {
    storage: multer.memoryStorage(),
    fileFilter(req, file, next) {
        const isPhoto = file.mimetype.startsWith('image/');
        if(isPhoto){
            next(null, true);
        } else {
            next({ message: 'The filetype isn\'t allowed'}, false);
        }
    }
};


exports.addStore = (req, res) => {
    res.render('editStore', { title: 'Add Store' });
};

exports.upload = multer(multerOptions).single('photo');

exports.resize = async (req, res, next) => {
    // check if there is no file to resize
    if(!req.file){
        next(); //skip to next middleware
        return;
    }
    const extension = req.file.mimetype.split('/')[1];
    req.body.photo = `${ uuid.v4() }.${ extension }`;

    //resizing the file
    const photo = await jimp.read(req.file.buffer);
    await photo.resize(800, jimp.AUTO);
    await photo.write(`./public/uploads/${req.body.photo}`);
    // once we have written the photo to our file system , keep going
    next();
};

exports.createStore = async (req, res) => {
    req.body.author = req.user._id;
    const store = await (new Store(req.body)).save();
    req.flash('success', `Successfully created ${store.name}. Care to leave a review?`)
    res.redirect(`/store/${store.slug}`);
};

exports.getStores = async (req, res) => {
    const page = req.params.page || 1;
    const limit = 6;
    const skip = (page * limit) - limit;
    //              1  *   4    -  4  gives us skip of 0 if we are on page 1
    //              2  *   4    -  4 gives us skip of 4 if we are on page 2
    const storesPromise = Store
        .find()
        .skip(skip)
        .limit(limit)
        .sort( { created: 'desc' } );

    const countPromise = Store.count();

    const [ stores, count ] = await Promise.all([storesPromise, countPromise]);

    const pages = Math.ceil(count / limit);

    //  agar pagesk buni nabu balam skip buni habu ema user axaina axir page
    if(!stores.length && skip) {
        req.flash('info', `Hey you asked for page ${page}. But that doesn't exist. So I put you on page ${pages}`);
        res.redirect(`/stores/page/${pages}`);
        return;
    }
    res.render('stores', { title: 'Stores', stores, page, pages, count });
};

const confirmOwener = (store, user) => {
    if(!store.author.equals(user._id)){
        throw Error('You must own a store in order to edit it!')
    }
}

exports.editStore = async (req, res) => {
    //1 Find the store given the ID
    const store = await Store.findOne({_id: req.params.id});
    //2 Confirm they are the owener of the store
    confirmOwener(store, req.user);
    //3 Render out the user form so the user can update thier store
    res.render('editStore', { title: `Edit ${store.name}`, store})
};

exports.updateStore =  async (req, res) => {
    // set a location data to be a Point
    req.body.location.type ='Point';
    //find and update store
    const store = await Store.findOneAndUpdate({_id: req.params.id}, req.body, {
        new: true, //return new store instead of old one
        runValidators: true
    }).exec();
    req.flash('success', `Successfully updated <strong>${store.name}</strong>. <a href='/store/${store.slug}'> View Store </a>`)
    // redirect them to store and tell them it worked
    res.redirect(`/stores/${store._id}/edit`)
};

exports.getStoreBySlug = async (req, res, next) => {
    const store = await Store.findOne({slug: req.params.slug}).populate('author reviews');
    if(!store) return next();
    res.render('store', { store, title: store.name });
};

// exports.getStoresByTag = async (req, res) => {
//     const tag = req.params.tag;
//     const tags = await Store.getTagsList();
//     res.render('tag', { tags, title: `Tags`, tag});
// };


exports.getStoresByTag = async (req, res) => {
    const tag = req.params.tag;
    //this tag query is for the main tag page, that makes it show all the stores
    const tagQuery = tag || { $exists:true }
    // we query tags and store promises at the same time, (we dont use await)
    const tagsPromise = Store.getTagsList();
    const storesPromise = Store.find({ tags: tagQuery });
    // then we await both promise results at the same time
    const [tags, stores] = await Promise.all([tagsPromise, storesPromise])

    res.render('tag', { stores, tags, title: `Tags`, tag});
};

exports.searchStores = async (req, res) => {
    const stores = await Store
    // find the stores that match
    .find({
        $text: {
            $search: req.query.q
        }
    }, {
        score: { $meta: 'textScore' }
    })
    // then sort them based on biggest score
    .sort({
        score: { $meta: 'textScore' }
    })
    //limit to only 5 result
    .limit(5);

    res.json(stores);
};


exports.mapStores = async (req, res) => {
    const coordinates = [req.query.lng, req.query.lat].map(parseFloat);
    const q = {
        location: {
            $near: {
                $geometry: {
                    type: 'Point',
                    coordinates
                },
                $maxDistance: 10000 // 10 km
            }
        }
    }

    const stores =  await Store.find(q).select('slug name description location photo').limit(10);
    res.json(stores);
};


exports.mapPage = (req, res) => {
    res.render('map', {title: 'Map'});
};

exports.heartStore = async (req, res) => {
    const hearts = req.user.hearts.map(obj => obj.toString());
    const operator = hearts.includes(req.params.id) ? '$pull' : '$addToSet';
    const user = await User
                        .findByIdAndUpdate(req.user._id,
                            { [operator]: { hearts: req.params.id } },
                            { new: true }
                        );
    res.json(user);
};


exports.getHearts = async (req, res) => {
    // where ID property of the store is in the hearts array of the user
    const stores = await Store.find({
        _id: { $in: req.user.hearts}
    });
    res.render('stores', {title: 'Hearted Stores', stores });
};


exports.getTopStores = async (req, res) => {
    const stores = await Store.getTopStores();
    res.render('topStores', { stores , title: '⭐ Top Stores!'});
}
